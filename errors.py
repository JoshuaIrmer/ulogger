# -*- coding: utf-8 -*-

class LoggerError(RuntimeError):
    """Base class for errors for this logger.
    """
    ...


class LevelNotFoundError(LoggerError):
    """An error, used when a level could not be found.
    """
    ...


class FormatingError(LoggerError):
    """An error, used when formating encounters an error.
    """
    ...
