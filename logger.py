# -*- coding: utf-8 -*-

from . import level as Level
from . import formats
from .errors import LevelNotFoundError, FormatingError
from .formating import Formating

_log_level = Level.NOSET
_format = formats.Default


def set_log_level(level: int):
    """Set global log level.

    Args:
        level (int): Log level value
    """
    global _log_level
    _log_level = level


def set_format(format: formats.FormatInterface):
    """Set global log formating.

    Args:
        format (formats.FormatInterface): Static formating class.
    """
    global _format
    _format = format


class _LogPrinter:
    """Class handeling data output
    """

    __name: str

    def __init__(self, name: str) -> None:
        self.__name = name

    def __format_default(self, level: int, data: dict[str, any]) -> str:
        """Output data in default format.

        Args:
            level (int): Log level vaule
            data (dict[str, any]): Log message related data

        Raises:
            FormatingError: An error occured while formating.

        Returns:
            str: Formated string, ready to be printed.
        """
        # set log level
        try:
            ln = Level.get_name(level)
        except LevelNotFoundError as e:
            raise FormatingError('could not determine log level name') from e

        if _format.colors:
            c = Level.get_color(level)
        else:
            c = Formating.Color.RESET

        s = f"[{c}{ln}{Formating.Color.RESET}"
        for _ in range(len(ln), len(Level.LONGEST_LEVEL_NAME)):
            s += ' '

        s += '] '

        # append name
        s += f'{self.__name}: '

        # filter message from data and append data
        if 'message' in data:
            msg = data['message']
        else:
            msg = ''

        for d in data:
            if d != 'message':
                s += f'{d}={data[d]} '

        # append message
        s += f"{Formating.Typeface.BOLD}{msg}{Formating.Typeface.RESET}"

        return s

    def print(self, level: int, data: dict[str, any]):
        """Route information to formating functions and print log messages.

        Args:
            level (int): Log level value
            data (dict[str, any]): Log message related data
        """
        if _format == formats.Default:
            print(self.__format_default(level, data))


class _LeveledLogger:
    """Class to collect log data on a log level basis.
    """

    __data: dict[str, any]

    def __init__(self, name: str, level: int) -> None:
        self.__level = level
        self.__printer = _LogPrinter(name)

        self.__data = {}

    def value(self, key: str, value: any) -> '_LeveledLogger':
        """Append a value with key to the log message.

        Args:
            key (str): Key of value
            value (any): Extra values to log.

        Returns:
            _LeveledLogger: self
        """
        self.__data[key] = value
        return self

    def send(self):
        """Send log data to printer.
        """
        if self.__level >= _log_level:
            self.__printer.print(self.__level, self.__data)

    def msg(self, msg: str):
        """Append log message and send.

        Args:
            msg (str): Log message
        """
        self.__data['message'] = msg
        self.send()


class Logger:
    """Logger class to log named logger and different logging values.
    """

    def __init__(self, name: str) -> None:
        self.name = name

    def debug(self) -> _LeveledLogger:
        """Create a debug logger.

        Returns:
            _LeveledLogger: Debug logger
        """
        return _LeveledLogger(self.name, Level.DEBUG)

    def info(self) -> _LeveledLogger:
        """Create a info logger.

        Returns:
            _LeveledLogger: Info logger
        """
        return _LeveledLogger(self.name, Level.INFO)

    def warn(self) -> _LeveledLogger:
        """Create a warn logger.

        Returns:
            _LeveledLogger: Warn logger
        """
        return _LeveledLogger(self.name, Level.WARN)

    def error(self) -> _LeveledLogger:
        """Create a error logger.

        Returns:
            _LeveledLogger: Error logger
        """
        return _LeveledLogger(self.name, Level.ERROR)

    def critical(self) -> _LeveledLogger:
        """Create a critical logger.

        Returns:
            _LeveledLogger: Critical logger
        """
        return _LeveledLogger(self.name, Level.CRITICAL)

    def get_child(self, name: str):
        """Create a child logger.
        A child logger is just a logger with the name of its parent
        and his own.

        Args:
            name (str): Name of child.

        Returns:
            Logger: Child logger
        """
        return Logger(f"{self.name}.{name}")


def get_logger(name: str) -> Logger:
    """Create a named logger.
    Mainly compatibility with pythons default logging.

    Args:
        name (str): Name of logger

    Returns:
        Logger: Named logger
    """
    return Logger(name)


__all__ = [
    "Logger",
    "_LeveledLogger"
]
