PROJECT_NAME=ulogger

.PHONY: lint format doc doc.html doc.pdf

lint:
	flake8 --exclude venv/

format:
	autopep8 --in-place --aggressive --recursive --exclude venv/ .

doc.html:
	pdoc --html --force .

doc.pdf:
	@pdoc --pdf --force . > pdf_tmp
	pandoc --metadata=title:${PROJECT_NAME} \
           --from=markdown+abbreviations+tex_math_single_backslash \
           --pdf-engine=xelatex --variable=mainfont:"DejaVu Sans" \
           --toc --toc-depth=4 --output=${PROJECT_NAME}.pdf pdf_tmp
	@rm -f pdf_tmp

doc: doc.html doc.pdf
