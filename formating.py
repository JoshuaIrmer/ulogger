# -*- coding: utf-8 -*-

class Formating:
    """Static class to safe ANSI escape codes for formating.
    """
    class Typeface:
        """Static class with typeface chaning ANSI escape codes.
        """
        RESET = '\033[0m'

        BOLD = '\033[1m'
        ITALIC = '\033[3m'
        UNDERLINE = '\033[4m'

    class Color:
        """Static class with color chaning ANSI escape codes.
        """
        RESET = '\033[0m'

        RED = '\033[91m'
        GREEN = '\033[92m'
        YELLOW = '\033[93m'
        BLUE = '\033[94m'
        PINK = '\033[95m'
        CYAN = '\033[96m'
