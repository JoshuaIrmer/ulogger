#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from ulogger import logger

log = logger.get_logger(__name__)


if __name__ == '__main__':
    log.critical().value("foo", "bar").msg('critical')
    log.error().value("bar", {"foob": "baz"}).msg('error')
    log.warn().value("limit", 5).msg('warn')
    log.info().msg('info')
    log.debug().msg('debug')
