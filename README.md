# ulogger

My MicroPython and Python3 compatible logger for my projects.

## Why?

Because I need a logger for my [MicroPython](https://micropython.org/) projects which need minimal or no [dependencies](requirements.txt) outside of basic python.

I prefer a structural logging approach, which I tried to implement by using the builder pattern like appending of logged values.

I wanted some fancy colors too.

## Why should you use it?

You probably shouldn't.

This is my logger for my projects. If I f*ck that up, it's on me and my worry.

It's only public, beacuse thats makes it easy to work with.

### But you want to use it?

Ok, feel free. You can use it of course.

I would be happy, if you use it and make some improvements, if you would share them.

## How I "install" it

Because I only use this for my MicroPython projects and don't want a real setup for it.

```bash
git submodule git@gitlab.com:JoshuaIrmer/ulogger.git path/in/project
```

Thats why its all just in one folder.

## Features

- Fancy colors: By default the log level is colored in of 4 + 1 colors.
- Typeface changes: Bold text to support messages.
- Log level padding: Log levels are padded to be visual pleasing.
- Structered logging: Builder pattern like value appending

```log
[CRITICAL] __main__: foo=bar critical
[ERROR   ] __main__: bar={'foob': 'baz'} error
[WARN    ] __main__: limit=5 warn
[INFO    ] __main__: info
[DEBUG   ] __main__: debug
```

Try [example.py](example.py) for usage and features.
