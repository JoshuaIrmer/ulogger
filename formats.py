# -*- coding: utf-8 -*-

class FormatInterface:
    """Static class used as base/interface for all formating classes.
    """
    colors: bool = True
    iso_time: bool = True


class Default(FormatInterface):
    """Static class for default formating.
    """
    ...
