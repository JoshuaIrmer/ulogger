# -*- coding: utf-8 -*-

from .errors import LevelNotFoundError
from .formating import Formating

"""Level module of the logger.
Responable for getting correct values, names and color formating.
"""

CRITICAL = 50
ERROR = 40
WARN = 30
INFO = 20
DEBUG = 10
NOSET = 10

NAMES: dict[int, str] = {
    50: 'CRITICAL',
    40: 'ERROR',
    30: 'WARN',
    20: 'INFO',
    10: 'DEBUG',
    0: ''
}

LONGEST_LEVEL_NAME = 'CRITICAL'  # Used for formating purposes.


def get_name(level: int) -> str:
    """Translate log level value into log level name.

    Args:
        level (int): Log level value

    Raises:
        LevelNotFoundError: Log level value has no corrosponding name.

    Returns:
        str: Log level name
    """
    try:
        return NAMES[level]
    except KeyError as e:
        raise LevelNotFoundError(
            f'level with value "${level}" not found') from e


def get_color(level: int) -> str:
    """Get color of log level ranges.

    Args:
        level (int): Log level value

    Returns:
        str: Color and typeface ANSI escape coedes.
    """
    if (level < 10):
        return ''
    elif (level < 20):
        return f"{Formating.Color.RESET}{Formating.Color.PINK}"
    elif (level < 30):
        return f"{Formating.Color.RESET}{Formating.Color.CYAN}"
    elif (level < 40):
        return f"{Formating.Color.RESET}{Formating.Color.YELLOW}"
    elif (level < 50):
        return f"{Formating.Color.RESET}{Formating.Color.RED}"
    elif (level >= 50):
        return f'{Formating.Color.RESET}{Formating.Typeface.BOLD}{Formating.Color.RED}'  # noqa: E501
